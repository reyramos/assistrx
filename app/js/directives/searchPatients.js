/**
 * Created by redroger on 1/21/14.
 */
angular.module('app').directive
	('searchPatients',['$window',function ($window) {
		var directive =
		{
			link: function (scope, element, attr) {
				var key;
				element.bind('keyup',function(){
					scope.currentPage = 0;
					key = element.val();
					if(key == ''){
						togglePagination(true)
					}else{
						togglePagination(false)
					}
					scope.$apply();
				})

				function togglePagination(val){
					scope.showPagination = val;
				}
			}
		};
		return directive;
	}]);