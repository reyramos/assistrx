// Require JS  Config File
require( {
			 baseUrl: '/js',
			 paths: {
				 'angular': '../lib/angular/index'
				 , 'angular-resource': '../lib/angular-resource/index'
				 , 'angular-route': '../lib/angular-route/index'
				 , 'angular-cookies': '../lib/angular-cookies/index'
				 , 'angular-sanitize': '../lib/angular-sanitize/index'
				 , 'angular-animate': '../lib/angular-animate/index'
				 , 'angular-touch': '../lib/angular-touch/index'
				 , 'jquery': '../lib/jquery/dist/jquery'
			 }, map: {
				 '*': { 'jquery': 'jquery' }, 'noconflict': { "jquery": "jquery" }
			 }, shim: {
				 'app': { 'deps': [
					   'angular'
					 , 'angular-route'
					 , 'angular-resource'
					 , 'angular-sanitize'
					 , 'angular-animate'
					 , 'angular-cookies'
					 , 'angular-touch'

				 ]}
				 , 'angular-route': { 'deps': ['angular', 'jquery'], exports: 'angular' }
				 , 'angular-resource': { 'deps': ['angular'] }
				 , 'angular-cookies': { 'deps': ['angular'] }
				 , 'angular-sanitize': { 'deps': ['angular'] }
				 , 'angular-animate': { 'deps': ['angular'] }
				 , 'angular-touch': { 'deps': ['angular'] }
				 , 'jquery': {
					 init: function ( $ ) {
						 return $.noConflict( true );
					 },
					 exports: 'jquery'
				 }
				 , 'routes': { 'deps': [
					 'app'
				 ]}
				, 'filters/globalFilters': { 'deps': ['app'] }
				, 'directives/searchPatients': { 'deps': ['app'] }
				, 'controllers/ApplicationController': {
					 'deps': [
						 'app'
				 ]}
				 , 'controllers/patientController': {
					 'deps': [
						 'app'
						 ,'services/appDataService'
					 ]}
				 , 'controllers/songController': {
					 'deps': [
						 'app'
						 ,'services/appDataService'
					 ]}
				 , 'controllers/reportController': {
					 'deps': [
						 'app'
						 ,'services/appDataService'
					 ]}
				 , 'services/appDataService': { 'deps': [
					 'app'
				 ]}
				}
			 }
		, [
			 'require'
			 , 'routes'
			 , 'filters/globalFilters'
			 , 'directives/searchPatients'
			 , 'controllers/ApplicationController'
			 , 'controllers/patientController'
			 , 'controllers/songController'
			 , 'controllers/reportController'
			 , 'services/appDataService'
		 ]
	, function ( require ) {
		return require(
			[
				'bootstrap'
			]
		)
	}
);