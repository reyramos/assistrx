/**
 * # ApplicationController
 *
 * Core application controller that includes all services needed to
 * kick-start the application
 */
angular.module('app').controller(
	'reportController',
	[
		'$scope'
		, '$log'
		, 'appDataService'
		, '$routeParams'
		, '$http'
		, function
			(
				$scope
				, $log
				, appDataService
				, $routeParams
				, $http
				) {


		var appServices = appDataService.getRequest();
		var  enums = appDataService.getEnums()
		$scope.patients = appServices.patients;
		$scope.predicate = 'patient_name'
		$scope.noSongs = false;
		$scope.song_data = [];

		function initialize(){
			var url = enums.requestTypes.songs;
				$http.get(url).success(function(data){

					if(data != 'false'){
						$scope.songs = data
						Object.keys(data).forEach(function(key) {
							$scope.song_data.push(JSON.parse ( data[key].song_data  ));
						});
					}else{
						$scope.noSongs = true;
					}
				})

		}

		$scope.routeURL = function(url){
			window.open(url,'itunes');
		}

		$scope.$on('$routeChangeSuccess', function(next, current) {
			initialize()
		});

	}]
)
