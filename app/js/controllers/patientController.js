/**
 * # ApplicationController
 *
 * Core application controller that includes all services needed to
 * kick-start the application
 */
angular.module('app').controller(
	'patientController',
	[
		'$scope'
		, '$log'
		, 'appDataService'
		, '$routeParams'
		, function
			(
				$scope
				, $log
				, appDataService
				) {


		var appServices = appDataService.getRequest();
		$scope.patients = appServices.patients;
		$scope.predicate = 'patient_name'
		$scope.pagination = $scope.patients.length / $scope.pageSize;


		$scope.setPage = function (index){

			var position = $scope.currentPage + 1;

			switch (index){
				case "f":
					if(position < $scope.pagination){
						$scope.currentPage++;
					}

					break;
				case "b":
					if(position > 1){
						$scope.currentPage--;
					}
					break;
				default:
					$scope.currentPage = index;
					break;
			}

		}


	}]
)
