/**
 * # ApplicationController
 *
 * Core application controller that includes all services needed to
 * kick-start the application
 */
angular.module('app').controller(
	'ApplicationController',
	[
		 '$scope'
		 , '$rootScope'
		, '$location'
		, function
		(
			 $scope
			 , $rootScope
			, $location
			) {


		$scope.currentPage = 0;
		$scope.pageSize = 10;
		$scope.grid = false;
		$scope.orderBy = false;
		$scope.setView = '';
		$scope.showPagination = true;
		$scope.showNavigation = false;

		$scope.routeTo = function( page ){

			if (typeof page == 'undefined') {
				$location.path( '/' );
			} else {
				$location.path( page );
			}
		};

	}]
)
