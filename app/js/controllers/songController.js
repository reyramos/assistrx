/**
 * # ApplicationController
 *
 * Core application controller that includes all services needed to
 * kick-start the application
 */
angular.module('app').controller(
	'songController',
	[
		'$scope'
		, '$log'
		, '$routeParams'
		, 'appDataService'
		, '$http'
		, function
			(
				$scope
				, $log
				, $routeParams
				, appDataService
				, $http
				) {


		var  enums = appDataService.getEnums()

		$scope.patient = {};
		$scope.song_data = false;
		$scope.noUser = false;

		var initialize = function(){
			var url = enums.requestTypes.songs;

			if($routeParams.patient_id && $routeParams.patient_id != ""){

				url = url+'?patient_id='+$routeParams.patient_id

				$http.get(url).success(function(data){

					if(data != 'false'){
						$scope.patient = data
						$scope.song_data = JSON.parse ( data.song_data  );
					}else{
						$scope.noUser = true;
					}
				})
			}else{
				// NO patient_id
				$scope.routeTo('/');
			}

		}()


		$scope.searchSong = function (){
			$http.jsonp('http://itunes.apple.com/search', {
				params: {
					"callback": "JSON_CALLBACK",
					"term": $scope.song_search,
					entity : 'song',
					limit : 25,
					country : 'US'
				}
			}).then(function(result) {
				if(result.status == 200){
					$scope.songs = result.data.results;
				}else{
					console.log('FAILED TO GET SONGS')
				}
			});

		}

		$scope.saveSong = function(song){

			var url = enums.requestTypes.ajax_controller+"?method=save_song_for_patient"
			$.post(url, {
				data : {
					patient_id : $scope.patient.patient_id,
					song_data : song
				}
			}, function(data) {

				var msg =  JSON.parse ( data  );

				$scope.safeApply( function () {
					$scope.song_data = song;
				})

				//hack, working on fixing this load of data
				window.location.reload()
			});
		}

		$scope.safeApply = function ( fn ) {
			var phase = this.$root.$$phase;
			if (phase == '$apply' || phase == '$digest') {
				if (fn && (typeof(fn) === 'function')) {
					fn();
				}
			} else {
				this.$apply( fn );
			}
		};
	}]
)
