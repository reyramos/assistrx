/**
 * # Global addDataService
 *
 * This will run before any controller to gather the
 * necessary data. It will call initApplication to gather
 * the RESTful services.
 *
 * This is an example of a Singleton Pattern within Angular
 *
 */

	// @TODO: Move this to enums directory and rename to globalEnums.
angular.module('app').service
	( 'appDataService',[
		'$q'
		, '$log'
		, '$http'
		, '$templateCache'
		, function(
			$q
			, $log
			, $http
			, $templateCache
			) {


			var  jsonRequest = []
				,enums = {}
				,appDataService = {};
			/**
			 * Initialize application and run before any controller
			 *
			 * @returns {promise}
			 */
			this.initApplication = function(){
				var defer = $q.defer()

				getRequestUrl('/api/enums.json' ).then(function(data){
					//get the requestTypes from enums
					jsonRequest = angular.isArray(data.requestTypes.patients)?data.requestTypes.patients:[data.requestTypes.patients];
					enums = data;

					console.log(enums)
					//once we have the jsonRequest
					getJsonConfigs().then(function(data){
						defer.resolve();
					});
				});

				return defer.promise;
			}

			//getters
			this.getRequest  = function(){
				return appDataService;
			}

			this.getEnums  = function(){
				return enums;
			}

			function getRequestUrl(url){
				var defer = $q.defer();
				$http.get(url).success(function(data){
					defer.resolve( data );
				})
				return defer.promise;
			}


			/**
			 * Gets the json file from $http.get(url) request, once success data is gathered it will update
			 * appDataService with new keys based on request url file name
			 * @param file
			 * @returns {exports.pending.promise|*|promise|defer.promise|promiseContainer.promise|a.fn.promise}
			 */
			function getJsonConfigs (){

				var deferred  = $q.defer(),url,key, handler = [];

				for(var i in jsonRequest){
					handler.push($http({method: "GET", url: jsonRequest[i], cache: $templateCache}))
				}

				$q.all( handler ).then(function(arrayOfResults){

					for(var i in arrayOfResults){
						if(arrayOfResults[i].status == 200){

							//TODO: make this dinamic for multiple requests
							url =  arrayOfResults[i].config.url;
							key = url.substring( url.lastIndexOf("/") + 1 ,url.indexOf('.'));

							//currently this is only supporting one request, view enums
							appDataService['patients'] = arrayOfResults[i].data
							deferred.resolve();
						}
					}

				}, function(data){
					$log.log('failed >',data);
				});

				return deferred .promise;
			}

		}]
	)