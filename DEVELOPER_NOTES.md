## AssistRx JavaScript Project ##

I chose to build this on AngularJS to demonstrate how quickly it can help with prototyping a site.
This site application demostrate the use of the following builds.

## Main features

- NodeJs - Is used to build a development enviroment with livereload, NodeJs is a full server stack to make fast, real-time application.
- Bower - Bower is a javascript dependency manager, <https://github.com/bower/bower>
- Grunt - Is a task runner, to help maintain and improve site production fast and reliable.
- YEOMAN - Some additional documentation from YEOMAN. <http://yeoman.io/>

I have also to demostrate a single index.html page development, which is great on mobile devices with low mobile data, to provide
a better experience to guest.


## Where the PHP? /api/ folder

I have decided to keep the PHP as a RESTful service, the reason to my madness is because of speed.  I want to provide a fast user experience to the
guest where they would only need to make one HTTP request to the server for every route.
The site is still making the necessary calls with ajax to gather the RESTful data, but the guest dont see this magic.
Instead the site will route, and if all the data has already been gather it will display to the DOM.

##Design and HTML5/CSS#
Use bootstrap to quickly mock up the site and angular to process the DOM elements.
The un-compile application is created using LESSJS, where once the application is compile it will create a single index, with the CSS, and Javascript
onto a single file. If you look further in the Javascript you will find the additional views hidden within the code.

## If I had more time with this: ##
I would have made a better design. My goal was to provide what was required from the README.pdf file.
I would have like to provide more Services within Angular, and properly done a RESTful service with ZendFrame work on the backend.

I would have like to incorporate more design and make it pretty with a responsive style, and possible add image upload of the Patients
from the mobile device.  This would have bee a nice features for the doctors that are with the patient to upload images to the database
with the patient information.
