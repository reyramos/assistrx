/**
 * Created with JetBrains PhpStorm.
 * User: r3droger
 * Date: 7/14/13
 * Time: 7:24 AM
 * To change this template use File | Settings | File Templates.
 */

'use strict';

var PORT = 9000;
var DOCUMENT_STRING_TITLE = "homework.powerdms.com";
var BOWER_DEST = "./app/lib";

var lrSnippet, modRewrite, mountFolder;

lrSnippet = require("grunt-contrib-livereload/lib/utils").livereloadSnippet;

modRewrite = require("connect-modrewrite");

mountFolder = function(connect, dir) {
	return connect["static"](require("path").resolve(dir));
};

module.exports = function(grunt) {
	require("matchdep").filter("grunt-*").concat(["gruntacular", 'grunt-contrib-compress']).forEach(grunt.loadNpmTasks);
	grunt.initConfig({
						 yeoman: {
							 app: "app",
							 dist: "dist"
						 },
						 watch: {
							 livereload: {
								 files: [
									 "<%= yeoman.app %>/*.html"
									 , "<%= yeoman.app %>/*.template"
									 , "{.tmp,<%= yeoman.app %>}/css/*.css"
									 , "{.tmp,<%= yeoman.app %>}/css/*.less"
									 , "{.tmp,<%= yeoman.app %>}/js/*.js"
									 , "{.tmp,<%= yeoman.app %>}/js/*/*.js"
									 , "{.tmp,<%= yeoman.app %>}/js/*/*/*.js"
									 , "{.tmp,<%= yeoman.app %>}/views/*.css"
									 , "<%= yeoman.app %>/images/*.{png,jpg,jpeg}"],
								 tasks: ["livereload", "template:dev"]
							 },
							 less: {
								 files: ["{.tmp,<%= yeoman.app %>}/css/*.less"],
								 tasks: ["less:dev", "livereload"]
							 }
						 },
						 less: {
							 dev: {
								 options: {
									 paths: ["<%= yeoman.app %>/css"]
								 },
								 files: {
									 ".tmp/css/main.css": "<%= yeoman.app %>/css/styles.less"
								 }
							 },
							 dist: {
								 options: {
									 paths: ["<%= yeoman.app %>/css", ".tmp/css"],
									 yuicompress: true
								 },
								 files: {
									 ".tmp/styles.css": "<%= yeoman.app %>/css/styles.less"
								 }
							 }
						 },
						 connect: {
							 options: {
								 hostname: "0.0.0.0",
								 port: PORT
							 },
							 livereload: {
								 options: {
									 middleware: function(connect) {
										 return [
											 lrSnippet
											 , modRewrite ( ["!png|jpg|gif|css|js|less|html|otf|ttf|eot|woff|svg$ /index.html [L]",
															 "^/?(api/.*(php))$ /$1 [L]"
															] )
											 , function(req, res, next) {
												 if (req.url.split('.').pop() === 'html') {
													 res.setHeader("Access-Control-Allow-Origin", "*");
													 res.setHeader("Access-Control-Allow-Headers", "X-Requested-With");
												 }
												 return next();
											 } , mountFolder ( connect, ".tmp" ), mountFolder ( connect, "app" )];
									 }
								 }
							 },
							 test: {
								 options: {
									 middleware: function(connect) {
										 return [
											 mountFolder(connect, ".tmp")
											 , mountFolder(connect, "app")];
									 }
								 }
							 },
							 dist: {
								 options: {
									 middleware: function(connect) {
										 return [
											 modRewrite ( ["!png|jpg|gif|css|js|less|html|otf|ttf$ /index.html [L]",
														   "^/?(api/.*(php))$ /$1 [L]"
														  ] )
											 , function(req, res, next) {
												 if (req.url.split('.').pop() === 'html') {
													 res.setHeader('Content-Encoding', 'gzip');
												 }
												 return next();
											 }, mountFolder(connect, "dist")
										 ];
									 }
								 }
							 }
						 },
						 open: {
							 server: {
								 url: "http://localhost:<%= connect.options.port %>/"
							 }
						 },
						 clean: {
							 dist: [".tmp", "<%= yeoman.dist %>/*"],
							 server: ".tmp"
						 },
						 jshint: {
							 options: {
								 jshintrc: ".jshintrc"
							 },
							 all: ["Gruntfile.js", "<%= yeoman.app %>/js/*.js", "app/test/spec/*.js"]
						 },
						 testacular: {
							 unit: {
								 configFile: "karma.coffee",
								 singleRun: true
							 }
						 },
						 dox: {
							 options: {
								 title: DOCUMENT_STRING_TITLE
							 },
							 files: {
								 src: ["<%= yeoman.app %>/js"],
								 dest: "<%= yeoman.app %>/docs"
							 }
						 },
						 ngtemplates: {
							 app: {
								 options: {
									 base: 'app/'
									 ,prepend: '/'
								 },
								 src: ["app/views/**.html", "app/views/*/*.html"],
								 dest: '.tmp/js/views.js'
							 }
						 },
						 requirejs: {
							 dist: {
								 options: {
									 baseUrl: "app/js",
									 findNestedDependencies: true,
									 logLevel: 0,
									 mainConfigFile: "app/js/main.js",
									 name: 'main',
									 include: ['../../.tmp/js/views.js'],
									 onBuildWrite: function(moduleName, path, contents) {
										 var modulesToExclude, shouldExcludeModule;
										 modulesToExclude = ['main', 'bootstrap'];
										 shouldExcludeModule = modulesToExclude.indexOf(moduleName) >= 0;
										 if (shouldExcludeModule) {
											 return '';
										 }
										 return contents;
									 },
									 optimize: "none",
									 out: ".tmp/scripts-require.js",
									 preserveLicenseComments: false,
									 skipModuleInsertion: true
								 }
							 }
						 },
						 concat: {
							 dist: {
								 src: ['.tmp/scripts-require.js', '.tmp/js/views.js', 'app/js/bootstrap.js'],
								 dest: ".tmp/scripts-concat.js"
							 }
						 },
						 uglify: {
							 options: {
								 mangle: false,
								 compress: true
							 },
							 dist: {
								 files: {
									 '.tmp/scripts.js': '.tmp/scripts-concat.js'
								 }
							 }
						 },
						 htmlmin: {
							 dist: {
								 options: {
									 removeComments: true,
									 removeCommentsFromCDATA: true,
									 collapseWhitespace: true
								 },
								 files: {
									 ".tmp/index.html": ".tmp/index-concat.html"
								 }
							 }
						 },
						 compress: {
							 dist: {
								 options: {
									 mode: 'gzip',
									 pretty: true
								 },
								 src: ['.tmp/index.html'],
								 dest: 'dist/index.html',
								 ext: '.html'
							 }
						 },
						 useminPrepare: {
							 html: "<%= yeoman.app %>/index.html",
							 options: {
								 dest: "<%= yeoman.dist %>"
							 }
						 },
						 imagemin: {
							 dist: {
								 files: [
									 {
										 expand: true,
										 cwd: "<%= yeoman.app %>/images",
										 src: "*.{png,jpg,jpeg}",
										 dest: "<%= yeoman.dist %>/images"
									 }
								 ]
							 }
						 },
						 copy: {
							 dist: {
								 files: [
									 {
										 expand: true,
										 dot: true,
										 cwd: "<%= yeoman.app %>",
										 dest: "<%= yeoman.dist %>",
										 src: ["*.html"
											 , "img/*.{png,gif,ico,txt,jpg}"
											 , "img/*/*.{png,gif,ico,txt,jpg}"
											 , "fonts/*.{otf,ttf,eot,woff,svg}"
											 , "fonts/**/*.{otf,ttf,eot,woff,svg}"
											 , "api/**/*.php"
											 , "api/*.php"
											 , ".htaccess"
										 ]
									 }
								 ]
							 },
							 dist2: {
								 files: [
									 {
										 expand: true,
										 dot: true,
										 cwd: ".tmp",
										 dest: "<%= yeoman.dist %>",
										 src: ["img/*.png", "*.html" , "api/*.{json,php}"]
									 }
								 ]
							 },
							 json:{
								 files:[
									 {
										 expand:true,
										 cwd:"api",
										 dest: ".tmp/api",
										 src:["**/*.{json,php}"]
									 }
								 ]
							 }
						 },
						 template: {
							 dev: {
								 files: {
									 ".tmp/index.html": "<%= yeoman.app %>/index.template"
								 },
								 environment: "dev"
							 },
							 dist: {
								 files: {
									 ".tmp/index-concat.html": "<%= yeoman.app %>/index.template"
								 },
								 environment: "dist",
								 css_sources: '<%= grunt.file.read(".tmp/styles.css") %>',
								 js_sources: '<%= grunt.file.read(".tmp/scripts.js") %>'
							 }
						 },
						 bower: {
							 install: {
								 options: {
									 targetDir: BOWER_DEST,
									 verbose: true,
									 install: true,
									 cleanup: true
								 }
							 }
						 }
					 });
	grunt.renameTask("regarde", "watch");

	grunt.registerTask("nodeServer", function() {
		return require("./api/server.js");
	});

	grunt.registerTask("server", function(target) {
		if (target === "dist") {
			return grunt.task.run(["livereload-start", 'nodeServer', "connect:dist:keepalive", "open"]);
		} else {
			return grunt.task.run([
									  "copy"
//									  , 'nodeServer'
									  , "livereload-start"
									  , "template:dev"
									  , "connect:livereload"
									  , "open"
									  , "watch"
								  ]);
		}
	});
	grunt.registerTask("server-only", function(target) {
		return grunt.task.run(["open", "watch"]);
	});
	grunt.registerTask("test", ["clean:server", "testacular"]);
	grunt.registerTask("build", [
		"clean:dist"
		, "ngtemplates"
		, "requirejs"
		, "concat:dist"
		, "uglify"
		, "less:dist"
		, "imagemin"
		, "template:dist"
		, "htmlmin"
		, "copy:json"
		, "copy:dist"
		, "copy:dist2"
	]);
	return grunt.registerTask("default", ["build"]);
};