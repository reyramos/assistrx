<?php
/**
 * namespace follow PSR-0 standard
 * all namespace must be lowerCase
 *
 * If using phpunit
 * phpunit --bootstrap autoload.php [testFile]
 */

spl_autoload_register(
	function($class) {
		static $path = "/models";
		static $classes = null;
		if ($classes === null) {
			$classes = array(
				'assistrx\\my_model' => '/my_model.php',
				'assistrx\\patient_model' => '/patient_model.php',
				'assistrx\\song_model' => '/song_model.php',
			);
		}

		$cn = strtolower($class);
		if (isset($classes[$cn])) {
			require __DIR__.$path.$classes[$cn];
		}
	}
);