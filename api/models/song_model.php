<?php
namespace AssistRx;

class song_model extends my_model
{
	public function __construct()
	{
		// constructing the parent gives us
		// access to the db through $this->db
		// which is a native php mysqli interface
		parent::__construct();
	}

	/**
	 * Returns arraoy of all the items from the song table
	 * @return array
	 */
	public function list_all()
	{
		$sql = $this->db->prepare('select * from songs');

		// expects that sql is a pdo prepared stmt
		$sql->execute();

		// http://php.net/manual/en/pdostatement.fetchall.php
		return $sql->fetchAll(\PDO::FETCH_CLASS);
	}

	/**
	 * Saves the song for patient, first it checks if the song existin within db, if it does then it will
	 * update the patient table with existing id from songs, else it will insert new song into song table.
	 * It also clean the song table, if a song is not being used by a patient it is deleted from the
	 * song table.
	 *
	 * @author Rey Ramos
	 * @since  140409
	 * @param $patient_id
	 * @param $song_data
	 * @return string
	 */
	public function save_song_for_patient($patient_id, $song_data)
	{

		//first check the if song already exist
		$hash = \md5(json_encode($song_data));

		$query = $this->db->prepare("
			select song_id
			FROM songs where
			song_hash=:song_hash");

		$query->execute(array(
			'song_hash' => $hash
		));

		$count = $query->rowCount();

		if ($count) {
			for ($i = 0; $row = $query->fetch(); $i++) {
				$song_id = $row['song_id'];
			}
		} else {
			$song_sql = $this->db->prepare("
            INSERT INTO songs
            (song_name, song_artist, song_data)
            VALUES (:song_name, :song_artist, :song_data)
            ");

			// expects that sql is a pdo prepared stmt
			$song_sql->execute(array(
				'song_name' => $song_data['trackName'],
				'song_artist' => $song_data['artistName'],
				'song_data' => json_encode($song_data)
			));

			$song_id = $this->db->lastInsertId();
		}


		$patient_sql = $this->db->prepare("
					UPDATE patients
					SET favorite_song_id = :song_id
					WHERE patient_id = :patient_id
				");

		$patient_sql->execute(array(
			'song_id' => $song_id,
			'patient_id' => $patient_id
		));

		$this->cleanSongsTable();

		return array("STATUS"=>true,"song_id" => $song_id,"patient_id" => $patient_id);

	}

	/**
	 * Will scan songs table and remove any unused song_id
	 */
	private function cleanSongsTable()
	{
		$query = $this->db->prepare("select song_id from songs left join patients on favorite_song_id=song_id where favorite_song_id is null");
		$query->execute();

		for ($i = 0; $row = $query->fetch(); $i++) {
			$song_id = $row['song_id'];
			$this->db->exec("DELETE FROM songs WHERE song_id = $song_id");
		}
	}
}